#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include<mpi.h> 

void kernal(int r,int x,int y,int len,int *orgData){
	int tx, ty;//poisition
	 //iteration
    for (int times = 1; times < 20000; times++)
    {
        //1:up，2:left，3:up，4:right
        switch (r)
        {
        case 1:
            tx = x;
            ty = y - 1;
            break;
        case 2:
            tx = x - 1;
            ty = y;
            break;
        case 3:
            tx = x;
            ty = y + 1;
            break;
        case 4:
            tx = x + 1;
            ty = y;
            break;
        default:
            tx = x;
            ty = y;
        }

       
        if (tx >= 0)
            if (tx < len)
                x = tx;
            else
                x = tx - len;
        else
            x = tx + len;

        if (ty >= 0)
            if (ty < len)
                y = ty;
            else
                y = ty - len;
        else
            y = ty + len;
		
        if (orgData[x*len+y] == 0)
        {
            orgData[x*len+y] = 1;
            r = r % 4 + 1;
        }
        else
        {
            orgData[x*len+y] = 0;
            r = (r + 2) % 4 + 1;
        }
    }
}
int main(int argc, char** argv)
{
	int x, y, r;//r value is 1/2/3/4
    int *orgData,*local;
	int len=10;
  //down
    r = 3;	
	//x,y is original value 
    x = 100;
    y = 100;
	int xl,yl,rank = 0, ss = 0;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &ss);
	len=strtol(argv[1],NULL,16);
	x=strtol(argv[2],NULL,16);
	y=strtol(argv[3],NULL,16);
	
	
    orgData=(int*)malloc(sizeof(int)*len*len);//
	if(orgData==NULL){
		printf("orgData malloc failed\n");
		return 0;
	}
    local=(int*)malloc(sizeof(int)*len*len);
if(rank==0){
    //start is die 
    for (int i = 0; i < len; i++)
        for (int j = 0; j < len; j++)
            orgData[i*len+j] = 0;
	 //set original poisition
    orgData[x*len+y] = 1;
}
   
 xl=len/ss;
   
 MPI_Scatter(orgData,xl*xl,MPI_INT,local,xl*xl,MPI_INT,0, MPI_COMM_WORLD);
	  
kernal(r,x,y,xl,local);
  MPI_Gather(local,xl*xl,MPI_INT,orgData,xl*xl,MPI_INT,0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
if(rank==0){	
     for (int i = 0; i < len; i++)
        for (int j = 0; j < len; j++)
            printf("%d  ",orgData[i*len+j]);
		printf("\n");
}
	free(orgData);
	orgData=NULL;
	free(local);
	local=NULL;
	MPI_Finalize();
	return 0;
}
